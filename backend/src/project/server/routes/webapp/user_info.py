from typing import Any

from fastapi import Form, HTTPException, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from jinja2 import pass_context

from project.db.models import User
from project.server import app

templates = Jinja2Templates(directory="/app/src/project/server/templates")


# https://stackoverflow.com/a/77835272
@pass_context
def urlx_for(context: dict, name: str, **path_params: Any) -> str:
    request: Request = context["request"]
    http_url = request.url_for(name, **path_params)
    if scheme := request.headers.get("x-forwarded-proto"):
        return http_url.replace(scheme=scheme)
    return http_url



@app.get("/api/telegram/webapp/{user_id}")
async def get_user_data(request: Request, user_id: int):
    current_user = await User.get_or_none(id=user_id)
    if not current_user:
        raise HTTPException(status_code=404, detail="Пользователь не найден")

    return templates.TemplateResponse(request=request, name="main.html", context={
        "user_count": await User.filter().count(),
        "user_name": current_user.real_name,
        "user_age": current_user.real_age,
        "user_id": user_id,
    })


@app.post("/api/telegram/webapp/{user_id}", response_class=HTMLResponse)
async def post_user_data(request: Request, user_id: int, new_name: str = Form(None), new_age: int = Form(None)):
    current_user = await User.get_or_none(id=user_id)
    if not current_user:
        raise HTTPException(status_code=404, detail="Пользователь не найден")

    if new_name:
        current_user.real_name = new_name
    if new_age:
        current_user.real_age = new_age
    await current_user.save()

    return templates.TemplateResponse(request=request, name="main.html", context={
        "user_count": await User.filter().count(),
        "user_name": current_user.real_name,
        "user_age": current_user.real_age,
        "user_id": user_id,
    })


templates.env.globals["url_for"] = urlx_for

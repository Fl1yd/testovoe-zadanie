from aiogram import types
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext

from project.db.models import User

from project.telegram import dp
from project.telegram.utils.answer import answer
from project.telegram.utils.handler import handler
from project.telegram.states import UserInfo


@dp.message(Command("start"))
@handler
async def start(message: types.Message, state: FSMContext):
    if not await User.get_or_none(id=message.from_user.id):
        await User.new(message.from_user)

    await answer(chat_id=message.chat.id, text="Здравствуйте! Введите ваше имя")
    return await state.set_state(UserInfo.name)

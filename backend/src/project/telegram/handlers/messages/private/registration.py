from aiogram import types
from aiogram.fsm.context import FSMContext
from aiogram.utils.keyboard import InlineKeyboardBuilder

from project.db.models import User

from project.settings import settings
from project.telegram import dp
from project.telegram.utils.answer import answer
from project.telegram.utils.handler import handler
from project.telegram.states import UserInfo


@dp.message(UserInfo.name)
@handler
async def user_name_handler(message: types.Message, state: FSMContext):
    current_user = await User.get_current()
    current_user.real_name = message.text
    await current_user.save(update_fields=["real_name"])

    await answer(chat_id=message.chat.id, text="Принял! Теперь введите ваш возраст")
    return await state.set_state(UserInfo.age)


@dp.message(UserInfo.age)
@handler
async def user_age_handler(message: types.Message, state: FSMContext):
    if not message.text.isdigit():
        return await answer(chat_id=message.chat.id, text="Возраст должен быть цифрой!")

    current_user = await User.get_current()
    current_user.real_age = int(message.text)
    await current_user.save(update_fields=["real_age"])

    builder = InlineKeyboardBuilder().add(
        types.InlineKeyboardButton(
            text="Перейти",
            web_app=types.WebAppInfo(url=f"https://{settings.DOMAIN}/api/telegram/webapp/{current_user.id}"))
    )

    await answer(chat_id=message.chat.id, text="Отлично! Вот данные:", reply_markup=builder.as_markup())
    return await state.clear()

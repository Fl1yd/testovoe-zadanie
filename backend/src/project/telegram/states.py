from aiogram.fsm.state import State, StatesGroup


class UserInfo(StatesGroup):
    name = State()
    age = State()
